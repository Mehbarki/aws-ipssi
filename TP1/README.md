# Semaine AWS - Ipssi DevOps

## TP1 : Utilisation de la console web de amazon pour instancier des vm et faire tourner un serveur web dessus

#### Pré-requis : Lancer une vm (Unix) avec la console web d'Amazon

### Etape 1 : Générer les clefs ssh et enregister la clef privé sur notre vm

### Etape 2 : Définir la VM avec un groupe de sécurité qui permet l'accès ssh (de base activé par default)

### Etape 3 : Se connecter avec un client SSH sur cette VM (ou il y a la clef privée)

### Etape 4 : Installer nginx 

### Etape 5 : Vérifier si le serveur web est accessible

### Etape 6 : Modifier les groupe de sécurité précédent pour que la page sur serveur web soit visible par le monde entier
