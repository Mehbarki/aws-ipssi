_pair" "kp_bastion" {
  key_name = "kp_bastion"
  # généré par ssh-keygen ...
  public_key = file("../ssh-keys/id_rsa_bastion.pub")
}

# Adapter le nom à l'usage
resource "aws_security_group" "sg_bastion" {
  name   = "sg_bastion"
  vpc_id = aws_vpc.vpc_example.id
  # en entrée
  # autorise ssh de partout
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    # on pourrait mettre l'ip sortante de notre réseau d'organisation
    cidr_blocks = ["0.0.0.0/0"]
  }
  # autorise icmp (ping)
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "bastion" {
  # Ubuntu 18.04 fournie par AWS
  ami                         = var.AMI[var.region]
  instance_type               = var.bastion_type
  key_name                    = "kp_bastion"
  vpc_security_group_ids      = [ aws_security_group.sg_internal.id,
                                  aws_security_group.sg_bastion.id ]
  subnet_id                   = aws_subnet.subnet_example.id
  private_ip                  = var.bastion_ip
  associate_public_ip_address = "true"
  user_data                   = file("../Scripts/bastion_init.sh")
  tags = {
    Name = "bastion"
  }
}

output "bastion_ip" {
  value = "${aws_instance.bastion.*.public_ip}"
}
# Adapter le nom à l'usage un nom doit être unique pour un compte AWS 
# donné
resource "aws_key_pair" "kp_wordpress_common" {
  key_name = "kp_wordpress_common"
  # généré par ssh-keygen ...
  public_key = file("../ssh-keys/id_rsa_wordpress_common.pub")
}
resource "aws_security_group" "sg_wordpress_front" {
  name = "sg_wordpress_front"
  vpc_id = aws_vpc.vpc_wordpress.id
  # en entrée autorise ssh de partout
  ingress {
    from_port = "22"
    to_port = "22"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # autorise http de partout
  ingress {
    from_port = "80"
    to_port = "80"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # autorise icmp (ping)
  ingress {
    from_port = 0
    to_port = 0
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "sg_wordpress_bdd" {
  name = "sg_wordpress_bdd"
  vpc_id = aws_vpc.vpc_wordpress.id
  # en entrée autorise ssh de partout
  ingress {
    from_port = "22"
    to_port = "22"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #autorise la co avec WP
  ingress {
    from_port = "0"
    to_port = "65535"
    protocol = "all"
    cidr_blocks = [ "10.0.0.0/24" ]
  }
  #autorise la co avec WPbdd
  ingress {
    from_port = "0"
    to_port = "65535"
    protocol = "all"
    cidr_blocks = ["10.0.0.0/24"]
  }
  # autorise http de partout
  ingress {
    from_port = "3305"
    to_port = "3305"
    protocol = "tcp"
    cidr_blocks = ["10.0.0.0/24"]
  }
  # autorise icmp (ping)
  ingress {
    from_port = 0
    to_port = 0
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_instance" "wordpress_front" {
  # Ubuntu 18.04 fournie par AWS ami = "ami-0bcc094591f354be2"
  ami = var.amis[var.region]
  instance_type = "t2.micro"
  key_name = "kp_wordpress_common"
  vpc_security_group_ids = [aws_security_group.sg_wordpress_front.id]
  subnet_id = aws_subnet.subnet_wordpress.id
  private_ip = "10.0.0.42"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/instance_init1.sh")
  tags = {
    Name = "wordpress_front"
  }
}
resource "aws_instance" "wordpress_bdd" {
  # Ubuntu 18.04 fournie par AWS
  ami = var.amis[var.region]
  instance_type = "t2.micro"
  key_name = "kp_wordpress_common"
  vpc_security_group_ids = [aws_security_group.sg_wordpress_bdd.id]
  subnet_id = aws_subnet.subnet_wordpress.id
  private_ip = "10.0.0.100"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/instance_init2.sh")
  tags = {
    Name = "wordpress_bdd"
  }
}
output "wordpress_front_ip" {
  value = "${aws_instance.wordpress_front.*.public_ip}"
}
output "wordpress_bdd_ip" {
  value = "${aws_instance.wordpress_bdd.*.public_ip}"
}
